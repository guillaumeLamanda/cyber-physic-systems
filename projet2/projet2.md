# Systèmes cyber-physique - projet 2

Ce projet consiste à réaliser un programme permettant de calculer la profondeur de fond en fonction du temps de retour et l'angle d'incidence.  

J'ai décider d'effectuer en [GO](https://golang.org/), ce langage étant promis à prendre le pas sur le C/C++.  
C'est le premier projet que j'effectue en GO, ce fût donc un test pour moi.  

Afin de vous permettre de vérifier sa compilation dans le cas ou vous n'auriez pas le langage d'installer, veuillez visionner [cette vidéo (preuve de commit + compilation + exécution)](https://drive.google.com/open?id=1VmTIsv1zYZPS258m8UqfKnAVLhj3Vt7g).  

---

Le programme peut être trouvé [ici](./golang/main.go)

Le programme ne fonctionne pas. Je penses que l'erreur vient du calcul de l'angle.

## Retour d'exécution

![execution](./execution.png)

Les angles affichés sont exprimé en gradient. Cependant, l'angle calculé ne correspond pas.  
En effet, on peut voir sur le retour de l'exécution que le second angle (donc le premier calculé) est supérieur à 1, ce qui est impossible.  

J'ai passé beaucoup de temps à comprendre le calcul de l'angle, et c'est vraiment frustrant de ne pas comprendre l'erreur. J'aurais apprécié quelques indications sur cette partie. 

## GoLang

Ce test avec le langage GoLang est plutôt concluant. L'expérience de développement est très bonne, et le linter nous oblige à respecter certaines règles de développement essencielles, comme commenter les variables globales, ou ne pas laisser de variables non utilisés.  
Si une règle n'est pas respecté, le programme ne compile pas.