#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <time.h>

int niveaux [] = {
  0, 1, 2, 3, 4,
  5, 6, 7, 8, 9, 
  10, 11, 12, 13, 14, 
  15
};

int profondeur[] = { 
  0, 10, 20, 30, 50, 
  75, 100, 125, 150, 200, 
  250, 300, 400, 500, 600, 
  700 
};

float celerite[] = {
  15.09, 15.06, 15.01, 14.85, 13.10, 
  10.53, 9.9, 9.57, 9.2, 8.71, 
  8.2, 7.5, 6.13, 5.17, 4.47, 
  4.11
};

int t, x, angle, temps;
char *p;

void init() {
  t=0;
  x=0;
}

void printHowTo() {
  printf("args : \n");
  printf("1. angle d’incidence de départ du faisceau\n");
  printf("2. range\n");
}

int main(int argc, char * argv[]) {
  
  if(argc != 3) printHowTo();
  angle = strtol(argv[1], &p, 10);
  temps = strtol(argv[2], &p, 10);
  
  printf("angle : %d\n", angle);
  printf("temps : %d\n", temps);
  
  exit(0);
}