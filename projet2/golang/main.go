package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

var temps, t, x int
var angle float64
var profondeurs []int

// T Temps de parcours
var T []float64

// Z Couches
var Z []float64

// C Celerite
var C []float64

// X Distrance transversales
var X []float64

// angles de sorties
var angles []float64

// P Propagation
var P float64

func g(i int) float64 {
	return (C[i] - C[i-1]) / (Z[i] - Z[i-1])
}

func rc(i int) float64 {
	return (C[0] / g(i) * math.Sin(angle*1 /** M_PI / 180 */))
}

func main() {

	Z := []int{
		0, 10, 20, 30, 50,
		75, 100, 125, 150, 200,
		250, 300, 400, 500, 600,
		700,
	}

	C := []float64{
		15.09, 15.06, 15.01, 14.85, 13.10,
		10.53, 9.9, 9.57, 9.2, 8.71,
		8.2, 7.5, 6.13, 5.17, 4.47,
		4.11,
	}

	if len(os.Args) != 3 {
		fmt.Println("usage : prog <angle> <temps>")
		os.Exit(1)
	}

	angle, error := strconv.Atoi(os.Args[1])
	if error != nil {
		fmt.Println("Args error")
	}
	temps, error := strconv.Atoi(os.Args[2])
	if error != nil {
		fmt.Println("Args error")
	}

	/* Initialisation */
	T := []float64{0}
	X := []float64{0}
	angles := []float64{math.Pi * float64(angle) / 180} // Angle en gradient
	i := 0

	/* De la couche Zi-1 Zi */
	for T[i] <= float64(temps) {
		// fmt.Println("for", T[i], temps, i, T[i] < float64(temps))
		i++
		if C[i] == C[i-1] {
			/* Angle de sortie de la couche */
			angles = append(angles, angles[i-1])

			/* Intant de sortie de la couche en seconde */
			T = append(T, T[i-1]+float64(Z[i]-Z[i-1])/(C[i]*math.Cos(float64(angles[i]))))

			/* Distance latérale parcourue en sortie de couche, en mètres */
			X = append(X, X[i-1]+math.Pow(0.5, float64(Z[i]-Z[i-1])*(1-math.Pow(2, math.Cos(float64(angles[i])))))/math.Cos(float64(angles[i])))
		} else {
			/* la propagation se fait selon un arc de cercle de rayon Phi */

			/* Gradient de célérité de la couche */
			Gi := float64(C[i]-C[i-1]) / float64(Z[i]-Z[i-1])
			fmt.Println("gradient de célérité", Gi)

			/* Rayon de courbure, en mètres */
			phi := C[0] / Gi * math.Sin(angles[0])
			fmt.Println("phi en mettre (rayon de courbure) : ", phi)

			/* Angle de sortie de la couche */
			angles = append(angles, math.Pow(0.5, 1-math.Pow(2, C[i]*math.Sin(angles[0])/C[0])))
			fmt.Println("angles", angles)
			/* Instant de sortie de la couche, en seconde */
			T = append(T, T[i-1]+math.Abs(math.Log((1+math.Cos(angles[i]))/(1-math.Cos(angles[i])))-math.Log((1+angles[i-1])/(1-angles[i-1]))/(2*Gi)))

			/* Distance latérale parcourue en sortie de couche, en mètres */
			X = append(X, X[i-1]+math.Abs(phi*(angles[i]-angles[i-1])))
		}
	}

	/* Traitement de la dernière couche */
	if t > temps {
		/* cas particulier de la couche isocélère - la propagation se fait par ligne droite */
		if C[i] == C[i-1] {
			angles[i] = angles[i+1]

			P = float64(Z[i-1]) + C[i]*(float64(temps)-T[i-1])*math.Cos(angles[i])

			X[i] = X[i-1] + C[i]*(float64(temps)-T[i-1])*math.Pow(0.5, 1-math.Pow(2, angles[i]))

			/* Sinon la propagation se fait selon un arc de cercle de rayon Phi */
		} else {
			var F = 2*g(i)*(float64(temps)-T[i-1]) + math.Log((1+math.Cos(angles[i-1]))/(1-math.Cos(angles[i-1])))

			/* cos(angle[i]) = */
			angles[i] = (math.Pow(F, math.E) - 1) / (math.Pow(F, math.E) + 1)
			/* sin(angle[i]) =  */
			angles[i] = (math.Pow(0.5, (1 - math.Pow(2, math.Cos(angles[i])))))

			X[i] = X[i-1] + math.Abs(math.Phi*(math.Cos(angles[i])-math.Cos(angles[i-1])))

			P = float64(Z[i-1]) + math.Abs(math.Phi*(math.Sin(angles[i])-math.Sin(angles[i-1])))
		}
	}

	fmt.Println("Propagation ", P)
	/* Distances relatives (along positif vers l'avant - across positif cers tribord) */
	// var along = X[len(X)] * math.Cos
}
