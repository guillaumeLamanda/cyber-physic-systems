﻿#include <cstdlib>
#include <iostream>
#include <ctime>


#ifdef WIN32 /* si vous êtes sous Windows */

#include <winsock2.h> 

#include <windows.h>

#elif defined (linux) /* si vous êtes sous Linux */

#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else /* sinon vous êtes sur une plateforme non supportée */

#error not defined for this platform

#endif

using namespace std;

#define PORT 51102

const char *hostname = "127.0.0.1";

SOCKET sock;
char buffer[256];
struct hostent *hostinfo = NULL;
SOCKADDR_IN server;

SOCKADDR_IN from;
#ifdef WIN32
int fromsize = sizeof from;
unsigned int serversize = sizeof server;
#else
socklen_t fromsize = sizeof from;
socklen_t serversize = sizeof server;
#endif

int messageSize=-1;

static void init(void)
{
#ifdef WIN32
    WSADATA wsa;
    int err = WSAStartup(MAKEWORD(2, 2), &wsa);
    if(err < 0)
    {
        puts("WSAStartup failed !");
        exit(EXIT_FAILURE);
    }
#endif
}

static void end(void)
{
#ifdef WIN32
    WSACleanup();
#endif
}

// fonction menu 
int menu()
{

	return choix;
}


int main(int argc, char *argv[])
{

    return EXIT_SUCCESS;
}




