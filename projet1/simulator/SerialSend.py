import serial
import threading



#Classe qui envoit les donnees sur un port
#Input: port
class SerialSend(threading.Thread):
    def __init__(self, port=0, fileToOpen=""):
        threading.Thread.__init__(self)
        self.port = port
        self.fileToOpen = fileToOpen
        self._stopevent = threading.Event( )
        self.s = serial.Serial(self.port, 9600, timeout=1)
    def run(self):
        while not self._stopevent.isSet():
            for line in open(self.fileToOpen).readlines():
                self.strToSend = line
                self.s.write(self.strToSend)
                self._stopevent.wait(1.0)
        print "le thread "+str(self.port) +" s'est termine proprement"
    def stop(self):
        self.s.close()
        self._stopevent.set()
