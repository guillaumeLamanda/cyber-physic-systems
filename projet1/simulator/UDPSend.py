import socket
import threading

HOST   = '127.0.0.1'
#'134.246.151.255'

#Classe qui envoit les donnees sur un port
#Input: port
class UDPSend(threading.Thread):
    def __init__(self, port=0, fileToOpen="", waittime=1.0):
        threading.Thread.__init__(self)
        self.port = port
        self.fileToOpen = fileToOpen
        self._stopevent = threading.Event( )
        self.waittime = waittime
    def run(self):
        s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        while not self._stopevent.isSet():
            for line in open(self.fileToOpen).readlines():
                self.strToSend = line
                s.sendto(self.strToSend,(HOST, self.port))
                if self._stopevent.isSet():
                    break
                self._stopevent.wait(self.waittime)
        print "le thread "+str(self.port) +" s'est termine proprement"
    def stop(self):
        self._stopevent.set( )
