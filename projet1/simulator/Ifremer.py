from UDPSend import UDPSend
import sys

print "Ifremer Demo Simulator"
print

gps = UDPSend(51102,"ashtech.dat")
gps.start()

sbe21 = UDPSend(51102,"sbe21.dat")
sbe21.start()

fluorometer = UDPSend(51102,"fluorometer.dat")
fluorometer.start()

print "Press y and enter if you want to stop"
s = sys.stdin.readline();
while s != 'y\n' :
     s = sys.stdin.readline()

gps.stop()
sbe21.stop()
fluorometer.stop()
