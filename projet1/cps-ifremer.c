#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <time.h>

#define EXIT_SUCCESS 0
#define PORT 51102

typedef int SOCKET;
typedef struct sockaddr SOCKADDR;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct in_addr IN_ADDR;
char buffer[1024];
int nbMessages = 0;
pthread_t thread; 
long unsigned start_time;
long totalSize = 0;
char gpgga[1024];

void *thread_socket(void *arg){
    /** socket */
    char * message;
    int messageSize; 
    SOCKET sock; 
    
    SOCKADDR_IN from = { 0 };
    int fromSize = sizeof from;
    
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock==-1) printf("Socket no created\n");
    
    from.sin_addr.s_addr = inet_addr("127.0.0.1");
    from.sin_family = AF_INET;
    from.sin_port = htons( PORT );
    
    if( bind(sock, (struct sockaddr*)&from, sizeof(from))<0 ){
        printf("connect error\n");
        exit(1);
    }
    
    start_time= time(NULL);
    while((messageSize=recvfrom(sock, buffer, sizeof(buffer)-1, 0, (SOCKADDR*)&from, &fromSize))>0){
        buffer[messageSize] = '\0';
        if(strncmp("$GPGGA",buffer,6) == 0){
            // printf("%s\n", buffer);
            strcpy(gpgga, buffer);
        }
        totalSize+=messageSize;
        nbMessages=nbMessages+1;
    }

    /* Pour enlever le warning */
    (void) arg;

    pthread_exit(NULL);
}

void printMenu(){
    printf("1. Nombre de trames\n");
    printf("2. Debit des trames (Ko/h)\n");
    printf("3. Capteur Ashtech\n");
    printf("4. Cadences d'émission\n");
    printf("5. Date et Géoréférencer 3 messages du fluo\n");
    printf("6. Quitter\n");

    printf("\n");
}

void launchSocket(){
    if(pthread_create(&thread, NULL, thread_socket, NULL) == -1) {
        perror("pthread_create");
        exit(1);
    }
}

void getLatLong(int opt){
    switch(opt){
        case 1:
            printf("%s\n", &buffer);
            break;
        default:
            
            break;
    }
}

void print_degres_decimaux(){
    char delimiters[] = ",";
    char * running = gpgga;
    strsep(&running, delimiters);
    strsep(&running, delimiters);
    char* lati = strsep(&running, delimiters);
    char* lati2 = lati;
    char* latiDir = strsep(&running, delimiters);
    printf("Latitude\t %s", lati);
    printf(" %s\n", latiDir);
    char* longi = strsep(&running, delimiters);
    char* longi2 = longi;
    printf("Longitude\t %s", longi);
    char* longiDir = strsep(&running, delimiters);
    printf(" %s\n", longiDir);
    printf("\n");
}

void print_degres_minute_secondes(){
    printf("WIP\n");
}

int main(int argc, char * argv[]){
    launchSocket();
    int rep=0;
    
    while(rep!=6){
        // system("clear");
        printMenu();
        scanf("%d", &rep);
        long unsigned selipsed = time(NULL)- start_time;
        switch(rep)
      {
          case 1:
            system("clear");
            printf("%d messages\n\n", nbMessages);
              break;

          case 2:
            system("clear");
            printf("%s\n", &buffer);
            printf("Debit des trames : %f Ko/h\n\n", (double) totalSize/1024 / selipsed/3600 );
              break;
          case 3:
              system("clear");
              printf("latitude/longitude en : \n");
              printf("\t1. degrés décimaux\n");
              printf("\t2. degrés min-décimales\n");
              printf("\t3. dégrés minutes-secondes\n");
              int rep ;
              scanf("%d", &rep);
              switch(rep){
                  case 1:
                    system("clear");
                    print_degres_decimaux();
                      break;
                  case 2:
                      system("clear");
                      printf("ok\n");
                      break;
                  case 3:
                      system("clear");
                      print_degres_minute_secondes();
                      break;
                  default:
                      system("clear");
                      break;
              }
              break;
          case 4:
            system("clear");
            printf("%f trames/s\n\n", (double) nbMessages/selipsed);
              break;
          case 5:
            printf("OK\n\n");
            break;
          case 6:
            printf("\n");
            printf("Bye bye...\n");
            break;

          default:
            printf("default\n");
            break;
      }
    }
  return EXIT_SUCCESS;
}
